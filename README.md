<!-- PROJECT LOGO -->
<br />
<p>
  <h1 text-align="center">BM2 Playground - Brain Muscle Memory for Calladium</h1>

<p text-align="center">
    <br />
    <br />
    <a href="https://rtonalli.gitlab.io/calladium/recursos-educativos/">View Site</a>
    ·
    <a href="#contact">Report Bug</a>
    ·
    <a href="#contact">Request Feature</a>
  </p>
</p>

---
<!-- TABLE OF CONTENTS -->
<details open="open">
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About the Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#roadmap">Roadmap</a></li>
    <li><a href="#contributing">Contributing</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
  </ol>
</details>

---
## About the Project

<img src="public/Screenshot-bm2.png" alt="screenshot" width="800" class="center">

This project implements a React solution to offer an interactive memory game for Calladium, a website that helps Spanish-speaking non-technical-savvy writers and students to learn how to use LaTeX and Overleaf for their writing projects.

The approach of this project is a repetition-based learning strategy to strengthen the brain muscle memory (BM2) by offering a gamified experience. Users enter commands to complete assigned tasks and learn the most common patterns and commands used in LaTeX and Git.

### Built With

The major frameworks used to build BM2 Playground are:

* [React](https://reactjs.org/)
* [Redux](https://redux.js.org/)
* [Lodash](https://lodash.com/)
* [Ant Design](https://ant.design/)

<!-- GETTING STARTED -->
## Getting Started

This section describes how to set up a local development environment to collaborate in the BM2 Playground project working locally.

### Prerequisites

You must have installed the following tools to work on the project locally:

* [Node.js](https://nodejs.org/en/)

    * To install Node.js, go to the [Downloads](https://nodejs.org/en/download/) page and download the source code or the installer according to your operative system.

* [Yarn](https://classic.yarnpkg.com/en/docs/getting-started/)
    
    * To install Yarn, run the following command:
        
        `npm install --global yarn`

### Installation

To install this project to work locally, follow the next steps:

1. Clone the repository with HTTPS using the following command:

    `git clone https://gitlab.com/santiago.torres/brain-memory-muscle-bm2.git`

2. Run the following command on the project's root directory to start the setup and install dependencies:

    `yarn`

3. Run the following command to run the local server:

    `yarn start`

    The application runs locally on port 3000. Open [http://localhost:3000](http://localhost:3000) to view the application in the browser.

4. Start coding.

<!-- USAGE EXAMPLES -->
## Usage

The BM2 Playground React solution uses a repetition-based learning strategy. The user interface randomly displays a task, and the end user must enter the corresponding LaTeX or Git command into a text box. The application verifies if the command is right or wrong and displays the correct answer. Also, the application can display a hint of the correct answer.

Developers add the tasks for end users into the `index.js` file, located inside the `src/components/customInput` directory. Each task is an item added as follows on the code:

```
export const learningPaths = {
  latex: {
    reset: false,
    items: [
      { id: 0, description: "Indica el inicio del cuerpo de un documento", hint: "\\begin{doc*****}", answer: "\\begin{document}", visited: false, recall: false }
```

The item structure is the following:

* **`id`.** It is the item's id.
* **`description`.** It is the description of the task.
* **`hint`.** It is the hint of the correct answer.
* **`answer`.** It is the correct answer.
* **`visited`.** It is a boolean value to determine if the item was visited by the user.
* **`recall`.** It is a boolean value that enables the user to visit the item again.

<!-- ROADMAP -->
## Roadmap

For a list of proposed features, see [Issues](https://gitlab.com/santiago.torres/brain-memory-muscle-bm2/-/issues) in the GitLab repository.

<!-- CONTRIBUTING -->
## Contributing

Any contribution is highly appreciated. To contribute with BM2 Playground, do the following:

1. Fork the project.
2. Create your feature branch using the following command:

    `git checkout -b name_feature_AmazingFeature`

3. Commit your changes using the following command:

    `git commit -m 'Add some AmazingFeature'`

4. Push to the branch using the following command:

    `git push origin feature/AmazingFeature`

5. Open a merge request in GitLab.

<!-- LICENSE -->
## License

Distributed under the BM2 Playground License. 

<!-- CONTACT -->
## Contact

To contact the Calladium team, you can send an email to Santiago Torres: <santiago.torres@wizeline.com>.