import {
  LOADING,
  UPDATE_PATH,
  ERROR
} from '../../utilities/constants';

const rootReducer = (state, action) => {

  switch (action.type) {
    case LOADING:
      return {
        ...state,
        loading: action.payload
      }
    case UPDATE_PATH:
      return {
        ...state,
        ongoingLearningPaths: action.payload
      }
    case ERROR:
      return {
        ...state,
        error: action.payload
      }
    default:
      return state
  }
}

export default rootReducer;
