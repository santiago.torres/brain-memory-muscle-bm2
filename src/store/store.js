import { createStore, applyMiddleware } from "redux";
import rootReducer from './reducers/rootReducer';
import { learningPaths as learningPathsSet } from '../utilities/learningPaths';
import thunk from 'redux-thunk';

const initialState = {
  mainTextContent: {
    title: 'Playground',
    description: 'Desarrolla la Brain Muscle Memory (BM2) mediante la repetición.',
  },
  learningPaths: learningPathsSet,
  ongoingLearningPaths: {}
}

const store = createStore(rootReducer, initialState, applyMiddleware(thunk));

export default store;
