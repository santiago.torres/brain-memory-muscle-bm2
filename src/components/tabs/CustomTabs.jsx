import { useEffect, useState } from "react";
import { Tabs } from "antd"
import { useSelector } from 'react-redux';
import { get } from 'lodash';
import CustomInput from "../customInput/customInput";
import { TexIcon } from '../../utilities/images/tex.icon';
import { GitIcon } from '../../utilities/images/git.icon';
import './customTabs.styles.scss'

const { TabPane } = Tabs;

function CustomTabs() {
  const learningPaths = useSelector(state => state?.learningPaths);
  const [selectedPath, setSelectedPath] = useState(1);

  const pickLearningPath = (key, _event) => {
    let learnThis;

    switch (key) {
      case "1":
        learnThis = get(learningPaths, 'latex', {})
        break;
      case "2":
        learnThis = get(learningPaths, 'git', {})
        break;
      default:
        learnThis = get(learningPaths, 'latex', {})
        break;
    }

    setSelectedPath(learnThis)
  }

  useEffect(() => {
    setSelectedPath(get(learningPaths, 'latex', {}));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div className="card-container">
      <Tabs
        centered
        size="large"
        type="card"
        defaultActiveKey="1"
        onTabClick={pickLearningPath}>
        <TabPane
          tab={
            <span>
              <TexIcon />
              LaTeX
            </span>
          }
          key="1"
        >
          <CustomInput
            generalLearningPaths={learningPaths}
            selectedLearningPath={selectedPath}
            pathName={'latex'} />
        </TabPane>
        <TabPane
          tab={
            <span>
              <GitIcon />
              Git
            </span>
          }
          key="2"
        >
          <CustomInput
            generalLearningPaths={learningPaths}
            selectedLearningPath={selectedPath}
            pathName={'git'} />
        </TabPane>
      </Tabs>
    </div>

  )
}

export default CustomTabs
