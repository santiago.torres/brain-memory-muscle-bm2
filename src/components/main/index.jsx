import { useSelector } from 'react-redux';
import { Fragment } from 'react';

import './main.styles.scss';

import { Typography } from 'antd';
import CustomTabs from '../tabs/CustomTabs';
import { BM2Icon } from '../../utilities/images/bm2.icon';
const { Text, Title } = Typography;

const Main = () => {
  const storeData = useSelector(state => state);
  const mainTextContent = useSelector(state => state?.mainTextContent);

  return (
    <section className='main-container'>
      {
        storeData?.loading
          ? <h1>Loading...</h1>
          : <Fragment>
            <Title className="header-title">
              <span className="bm2icon">{<BM2Icon />}</span>
              {mainTextContent?.title}</Title>
            <Text className="header-description">{mainTextContent?.description}</Text>
            <CustomTabs />
          </Fragment>
      }
    </section>
  );
}

export default Main;
