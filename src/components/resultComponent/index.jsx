import uuid from 'react-uuid';
import { Card, Typography } from 'antd';
import { map, split } from 'lodash';

const { Text, Title } = Typography;

export const ResultComponent = ({ userInput, description, answer }) => {
  const inputChecker = () => {
    const green = '#1DA373';
    const red = '#D14064';
    const splittedStr = split(userInput, '');

    return map(splittedStr, (userChar, idx) => {
      const controlChar = answer[idx];
      return (
        <span
          id={uuid()}
          style={{ color: userChar === controlChar ? green : red }}>
          {userChar}
        </span>
      )
    });
  }

  return (
    <Card id={uuid()} className="resultcard">
      <Title level={4}>{inputChecker()}</Title>
      <Text>{description}</Text>
      <br />
      <Text type="success">{answer}</Text>
    </Card>
  )
}