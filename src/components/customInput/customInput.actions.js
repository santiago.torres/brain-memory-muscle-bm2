import {
  LOADING,
  UPDATE_PATH,
  ERROR
} from '../../utilities/constants';

const actions = {
  loading: (boolean = false) => ({
    type: LOADING,
    payload: boolean
  }),
  updatePath: (data) => ({
    type: UPDATE_PATH,
    payload: data
  }),
  error: (data) => ({
    type: ERROR,
    payload: data
  })
}

const { updatePath } = actions;

const updatePathAction = (data) => {
  return dispatch => {
    dispatch(updatePath(data));
  }
}

export { updatePathAction };