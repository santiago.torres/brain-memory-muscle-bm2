import { useState, useEffect, Fragment } from 'react';
import { EnterOutlined, EyeInvisibleOutlined, EyeOutlined } from '@ant-design/icons';
import { Form, Button, Divider, Input, Tooltip } from 'antd';
import { get, sample, isEmpty, map } from 'lodash';
import Title from 'antd/lib/typography/Title';
import './customInput.styles.scss'
import { useDispatch, useSelector } from 'react-redux';
import { updatePathAction } from './customInput.actions';
import { ResultComponent } from '../resultComponent';
import confetti from 'canvas-confetti';

const CustomInput = ({ generalLearningPaths, selectedLearningPath, pathName }) => {
  const [form] = Form.useForm();
  const [sampledItem, setSampledItem] = useState(null);
  const [hintIsActive, setHintIsActive] = useState(false);
  const [cardStack, setCardStack] = useState([]);
  const learningItems = get(selectedLearningPath, 'items', []);
  const ongoing = useSelector(state => state?.ongoingLearningPaths);
  const dispatch = useDispatch();

  const pickSample = () => {
    const pickedSample = sample(learningItems);
    setSampledItem(pickedSample);
  }

  const cardStackGenerator = () => {
    return map(cardStack, (item) => {
      return (
        <ResultComponent 
          userInput={item?.userInput}
          description={item?.description}
          answer={item?.answer}
        />
      )
    });
  }

  const hintButtonClickHandler = () =>
    hintIsActive ? setHintIsActive(false) : setHintIsActive(true);

  const enterKeyPressHandler = (event, questionId) => {
    if (event.keyCode === 13) {
      learningItems[questionId].visited = true;
      learningItems[questionId].userInput = event?.target?.value?.trim();
      
      const userInput = learningItems[questionId]?.userInput;
      const answer = learningItems[questionId]?.answer;
      
      form.resetFields();
      
      dispatch(updatePathAction({ ...generalLearningPaths, [pathName]: { items: learningItems } }));
      pickSample();
      setCardStack([learningItems[questionId], ...cardStack]);
      
      if (userInput === answer) {
        confetti({ particleCount: 150 });
      }
    }
  };

  useEffect(() => {
    pickSample(learningItems, setSampledItem);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [learningItems, selectedLearningPath]);

  useEffect(() => {
    isEmpty(ongoing)
      ? dispatch(updatePathAction(generalLearningPaths))
      : dispatch(updatePathAction(ongoing));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <Fragment>
      <Title level={4} style={{ textAlign: 'center' }}>{sampledItem?.description}</Title>
      <Form className="custominput-container" name="basic" form={form}>
        <Form.Item
          name="hideBtn">
          <Tooltip title="Muestra/Oculta una pista">
            <Button
              type="dashed"
              size="large"
              icon={hintIsActive ? <EyeInvisibleOutlined /> : <EyeOutlined />}
              onClick={hintButtonClickHandler}
            />
          </Tooltip>
        </Form.Item>
        <Form.Item
          name="com"
          style={{ width: "100%" }}
          required={[{ required: true, message: 'Escribe el comando aquí.' }]}>
          <Input
            size="large"
            placeholder={hintIsActive ? sampledItem?.hint : ''}
            onPressEnter={(e) => enterKeyPressHandler(e, sampledItem?.id)}
            autoFocus
            suffix={
              <EnterOutlined style={{ color: 'rgba(0,0,0,.45)' }} />
            }
          />
        </Form.Item>
      </Form>
      {cardStack.length ? <Divider dashed id="divider" /> : null}
      {cardStack.length ? cardStackGenerator() : null}

    </Fragment>
  )
}

export default CustomInput;
