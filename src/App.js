import { Provider } from "react-redux";
import Main from "./components/main";
import store from './store/store';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";

import './utilities/Normalize.scss';


function App() {
  console.info("BM2 is Online 🚀")
  return (
    <Provider store={store}>
      <Router basename='/bm2'>
        <Switch>
          <Route path="/" exact> <Main /> </Route>
        </Switch>
      </Router>
    </Provider>
  );
}

export default App;
