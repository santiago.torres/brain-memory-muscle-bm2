const LOADING = 'LOADING';
const UPDATE_PATH = 'UPDATE_PATH';
const ERROR = 'ERROR';

export {
  LOADING,
  UPDATE_PATH,
  ERROR
};
